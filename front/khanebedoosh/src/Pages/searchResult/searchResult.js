import React from 'react';
import {Footer, Navbar} from '../landing/landing';
import './searchResult.css'
import './navbar.css';
import './area.css';
import './house.css';

function Area(props) {
  return (
    <div className="area">
      <div className="search-result">{props.text}</div>
    </div>
  );
}
function House(props) {

  let buttonClass = '';
  let iconClass = '';
  let buttonText = '';
  const areaText = props.area + 'متر مربع';
  let priceText = "قیمت ";
  let mortgageText = "رهن ";
  let rentText = "اجاره ";
  let priceElement = '';

  if (props.dealType === "sell") {
    buttonClass = "button-purple";
    iconClass = "icon-magenta";
    buttonText = "فروش";
    priceText = priceText + props.price;
    priceElement = <div>
      <div className="sub-image-text sub-image-text-right-bottom">{priceText}</div>
      <div className="sub-image-text sub-image-text-right-bottom set-gray">تومان</div>
    </div>;
  }
  if (props.dealType === "rent") {
    buttonClass = "button-red";
    iconClass = "icon-red";
    buttonText = "رهن و اجاره";
    mortgageText = mortgageText + props.mortgage;
    rentText = rentText + props.rent;
    priceElement = <div>
      <div className="sub-image-text sub-image-text-right-bottom">{mortgageText}</div>
      <div className="sub-image-text sub-image-text-right-bottom set-gray">تومان</div>
      <div className="sub-image-text sub-image-text-left-bottom set-gray">تومان</div>
      <div className="sub-image-text sub-image-text-left-bottom">{rentText}</div>
    </div>;
  }
  buttonClass = buttonClass.concat(" sub-button");
  iconClass = iconClass.concat(" fa fa-map-marker my-icon");

  return (
    <div className="col-md-4 col-12">

      <div className="my-house">
        <form action={"/House"}>
          <input hidden name="houseId" value={props.houseId}/>
          <button className={buttonClass + " btn"}>{buttonText}</button>
        </form>
        <img src={props.imageUrl} className="my-image" alt="house image"/>
        <div className="sub-image">
          <div className="sub-image-text sub-image-text-right">{areaText}</div>
          <div className="sub-image-text sub-image-text-left">{props.houseAddress}</div>
          <i className={iconClass}/>
          <hr/>
          {priceElement}
        </div>
      </div>
    </div>
  );
}
function HouseList(props) {
  let output = [];
  let temp = [];
  for (let i = 0; i < props.houseDetails.length / 2; i++) {
    temp = [];
    for (let j = 0; j < 2; j++) {
      if (typeof props.houseDetails[2 * i + j] !== "undefined") {
        if (props.houseDetails[2 * i + j].dealType == "0") {
          temp.push(
            <House
              dealType={"sell"} imageUrl={props.houseDetails[2 * i + j].imageUrl}
              area={props.houseDetails[2 * i + j].area}
              houseAddress={props.houseDetails[2 * i + j].address}
              price={props.houseDetails[2 * i + j].sellPrice}
              houseId={props.houseDetails[2 * i + j].uuid}
            />);
        }
        if (props.houseDetails[2 * i + j].dealType == "1") {
          temp.push(<House dealType={"rent"} imageUrl={props.houseDetails[2 * i + j].imageUrl}
                           area={props.houseDetails[2 * i + j].area}
                           houseAddress={props.houseDetails[2 * i + j].address}
                           mortgage={props.houseDetails[2 * i + j].basePrice}
                           rent={props.houseDetails[2 * i + j].rentPrice}
                           houseId={props.houseDetails[2 * i + j].uuid}/>);
        }
      }
    }
    let last_element = "";
    if ((2 * i + 1) == props.houseDetails.length)
      last_element = <div className="col-md-4 col-12"/>;
    output.push(<div className="row set-center">
      {temp}
      {last_element}
    </div>);
  }

  return (
    <div className="container-fluid text-center">
      {output}
    </div>
  );
}
class SearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      houses: []
    };
  }
  componentDidMount() {
    const url = new URL(window.location.href);
    const leastArea = url.searchParams.get("leastArea");
    const mostPrice = url.searchParams.get("mostPrice");
    const dealType = url.searchParams.get("dealType");
    const buildingType = url.searchParams.get("buildingType");
    const dest = 'http://localhost:4000/search';

    fetch(dest, {
      method: 'POST',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        leastArea,
        mostPrice,
        dealType,
        buildingType
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            houses: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    return (
      <div>
        <Navbar/>
        <Area text="نتایج جستجو"/>
        <p align="center" className="guide set-center">برای مشاهده اطلاعات بیشتر درباره هر ملک روی آن کلیک کنید</p>
        <HouseList houseDetails={this.state.houses}/>
        <Footer/>
      </div>
    );
  }
}

export {Area, HouseList, SearchResult, House};