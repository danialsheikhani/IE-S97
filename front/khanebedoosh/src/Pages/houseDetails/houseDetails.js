import React from 'react';
import {Area} from '../searchResult/searchResult';
import {Navbar, Footer} from "../landing/landing";
import './houseDetails.css';
class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber:''
    }
  }
  handleClick(){
    const url = new URL(window.location.href);
    const houseId = url.searchParams.get("houseId");
    const dest = 'http://localhost:4000/buy/' + houseId;
    fetch(dest, {
      headers: {'Authorization': window.sessionStorage.token},
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            phoneNumber: result.msg
          });
        },
        (error) => {
          this.setState({
            error
          });
        }
      );
  }
  render() {
    let buttonClass = 'no-click';
    let buttonText = '';
    if (this.props.details.dealType === 0) {
      buttonClass += " button-purple";
      buttonText ='فروش';
    }
    if (this.props.details.dealType === 1) {
      buttonClass += " button-red";
      buttonText ='رهن و اجاره';
    }
    return (
      <div className="container-fluid" id="house-details">
        <div className="row set-center">
          <div className="col-md-5 col-12 text-right">
            <div className="row">
              <div className="col-6">
                <button className={buttonClass + " btn"}>{buttonText}</button>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                شماره مالک/مشاور
              </div>
              <div className="col-6">
                {this.state.phoneNumber}
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                نوع ساختمان
              </div>
              <div className="col-6">
                {this.props.details.buildingType}
              </div>
            </div>
            {this.props.details.dealType === 0 &&
              <div className="row">
                <div className="col-6">
                  قیمت
                </div>
                <div className="col-6">
                  {this.props.details.price.sellPrice}
                </div>
              </div>
            }
            {this.props.details.dealType === 1 &&
              <div>
                <div className="row">
                  <div className="col-6">رهن</div>
                  <div className="col-6">{this.props.details.price.basePrice}</div>
                </div>
                <div className="row">
                  <div className="col-6">اجاره</div>
                  <div className="col-6">{this.props.details.price.rentPrice}</div>
                  </div>
              </div>
            }
            <div className="row">
              <div className="col-6">آدرس</div>
              <div className="col-6">{this.props.details.address}</div>
            </div>
            <div className="row">
              <div className="col-6">
                متراژ
              </div>
              <div className="col-6">
                {this.props.details.area}
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                توضیحات
              </div>
              <div className="col-6">
                {this.props.details.description}
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">
            <div className="row set-center">
              <img src={this.props.details.imageURL} alt="عکس خانه"/>
            </div>
            <div className="row set-center">
              <button className="btn btn-search" id={"search-button"} onClick={(e) => this.handleClick(e)}>مشاهده تلفن</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class HouseDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      details: []
    }
  }
  componentDidMount() {
    const url = new URL(window.location.href);
    const houseId = url.searchParams.get("houseId");
    const dest = 'http://localhost:4000/house/' + houseId;
    fetch(dest)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            details: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    return (
      <div>
        <Navbar/>
        <Area text="مشخصات کامل ملک"/>
        <Details details={this.state.details}/>
        <Footer/>
      </div>
    );
  }
}

export {HouseDetails};