import React from 'react';
import './header.css';
import './navbar.css';
import './searchbox.css';
import './whyus.css';
import './footer.css';

function Dropdown(props) {
  return (
      <div className="dropdown col-md-3 col-5">
        <div className="myspan">ناحیه کاربری</div>
        <i className="fa fa-smile-o text-right"/>
        <div className="dropdown-content container-fluid">
          <div className="row">
            <div className="text-right set-black col-12">{props.username}</div>
          </div>
          <div className="row">
            <div className="col-6 credit">اعتبار</div>
            <div className="col-6 currency">{props.balance} تومان</div>
          </div>
          <form action="/addCredit">
            <button className="btn drop-button my-button">افزایش اعتبار</button>
          </form>
        </div>
      </div>
  );
}
class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      balance: 0,
    };
  }
  componentDidMount() {

    const url = "http://localhost:4000/user";
    fetch(url, {
      headers: {'Authorization': window.sessionStorage.token},
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            username: result.username,
            balance: result.balance
          });
        },
        (error) => {
          console.log(error.toString());
          this.setState({
            error
          });
        }
      )
  }
  render() {
    let url = '/Landing';
    if (this.props.page === "landing"){
      return(
        <div className="navbar-landing">
          <Dropdown username={this.state.username} balance={this.state.balance}/>
        </div>
      );
    } else {
      return (
        <div className="my-navbar">
          <a href={url}>
            <img className="aks" src="logo.png" alt=""/>
          </a>
          <a href={url} className="house">خانه به دوش</a>
          <Dropdown username={this.state.username} balance={this.state.balance}/>
        </div>
      );
    }
  }
}


function Header() {
  return <div className="text-center ">
    <div className="row container-fluid">
      <div className="col-md-12">
        <img src="logo.png" className="main-logo" alt="لوگو"/>
      </div>
    </div>
    <div className="row container-fluid">
      <div className="col-12 text-logo">
        <h2>خانه به دوش</h2>
      </div>
    </div>
  </div>;
}

class SearchBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      buildingType: '',
      leastArea: '',
      mostPrice: '',
      dealType: '',
      buy: '0',
      rent: '1',
      buildingTypeValid: false,
      leastAreaValid: false,
      mostPriceValid: false,
      dealTypeValid: false,
      formErrors: {
        buildingType: '',
        leastArea: '',
        mostPrice: '',
        dealType: '',
      }
    }
  }

  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(
      {[name]: value},
      () => {
        this.validateField(name, value)
      }
    );
  }
  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let mostPriceValid = this.state.mostPriceValid;
    let leastAreaValid = this.state.leastAreaValid;
    let dealTypeValid = this.state.dealTypeValid;
    let buildingTypeValid = this.state.buildingType;

    switch (fieldName) {
      case 'mostPrice':
        mostPriceValid = !isNaN(value);
        fieldValidationErrors.mostPrice = 'Should be an integer number';
        break;
      case 'leastArea':
        leastAreaValid = !isNaN(value);
        fieldValidationErrors.leastArea = 'Should be an integer number';
        break;
      case 'dealType':
        dealTypeValid = (value === '1' || value === '0');
        break;
      case 'buildingType':
        buildingTypeValid = (value === 'آپارتمان' || value === 'ویلایی');
        break;
      default:
        break;
    }

    this.setState({
      formErrors: fieldValidationErrors,
      mostPriceValid: mostPriceValid,
      leastAreaValid: leastAreaValid,
      dealTypeValid: dealTypeValid,
      buildingTypeValid: buildingTypeValid,
    }, this.validateForm);


  }
  validateForm() {
    this.setState({
      formValid:
      this.state.mostPriceValid &&
      this.state.leastAreaValid &&
      this.state.buildingType &&
      this.state.dealTypeValid
    });
  }
  render() {

    return (
      <form action="/SearchResult">
        <div className="row set-center text-center">
          <div className="col-md-9 col-10 search-box">
            <div className="row set-center search-row">
              <div className="col-md-4 col-12 ">
                <div className="row unit">
                  <p>&nbsp;</p>
                </div>
                <div className="row">
                  <select name="buildingType" className="house-field" onChange={(event) => this.handleUserInput(event)}>
                    <option selected hidden disabled>نوع ملک</option>
                    <option value="آپارتمان">آپارتمان</option>
                    <option value="ویلایی">ویلایی</option>
                  </select>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="row unit">
                  <p>تومان</p>
                </div>
                <div className="row">
                  <input name="mostPrice" className="house-field " type="number" placeholder="حداکثر قیمت"
                         value={this.state.mostPrice} onChange={(event) => this.handleUserInput(event)}/>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="row unit">
                  <p>متر مربع</p>
                </div>
                <div className="row">
                  <input name="leastArea" className="house-field" type="number" placeholder="حداقل متراژ"
                         value={this.state.leastArea} onChange={(event) => this.handleUserInput(event)}/>
                </div>
              </div>
            </div>
            <div className="row set-center search-row">
              <div className="col-md-6 col-12 text-right">
                <div className="row">
                  <input name="dealType" className="btn-radio" type="radio" value="1"
                         onChange={(event) => this.handleUserInput(event)}/>
                  <span>رهن و اجاره</span>
                  <input name="dealType" className="btn-radio" type="radio" value="0"
                         onChange={(event) => this.handleUserInput(event)}/>
                  <span>خرید</span>
                </div>
              </div>
              <div className="col-md-6 col-12">
                <div className="row">
                  <button className="btn-search btn" disabled={!this.state.formValid}>جستجو
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

function AddHouse() {
  return <div className="row set-center text-center">
    <div className="col-md-9 col-10 search-box">
      <h5>صاحب خانه هستید؟ خانه خود را ثبت کنید</h5>
    </div>
  </div>;
}

function UpperPart(props) {
  return (

    <div className="slideshow container-fluid">
      <Navbar page={"landing"}/>
      <Header/>
      <SearchBox/>
      <AddHouse/>
    </div>
  );
}

function WhyUsLogos() {
  return (
    <div className="row set-center text-center">
      <div className="col-md-3 col-8 light-box">
        <div>
          <img src="/icons/726446.svg"
               className="img-fluid logo" alt="آسان"/>
          <h2>آسان</h2>
          <h5>به سادگی صاحب خانه شوید.</h5>
        </div>
      </div>
      <div className="col-md-3 col-8 light-box">
        <div>
          <img src="/icons/726488.svg"
               className="img-fluid logo" alt="مطمئن"/>
          <h2>مطمئن</h2>
          <h5>با خیال راحت به دنبال خانه بگردید.</h5>
        </div>
      </div>
      <div className="col-md-3 col-8 light-box">
        <div>
          <img src="/icons/726499.svg"
               className="img-fluid logo" alt="گسترده"/>
          <h2>گسترده</h2>
          <h5>در منطقه مورد علاقه خود صاحب خانه شوید.</h5>
        </div>
      </div>
    </div>
  );
}

function WhyUsText() {
  return (
    <div className="row set-center text-right why-khanebedoosh">
      <div className="col-md-5 col-8">
        <h1>چرا خانه به دوش؟</h1>
        <ul className="why-khanebedoosh-li">
          <li>
            <p><i className="fa fa-check-circle set-purple"/>اطلاعات کامل و صحیح از املاک قابل معامله</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>بدون محدودیت، ۲۴ ساعته و در تمام ایام هفته</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>جستجوی هوشمند ملک، صرفه‌جویی در زمان</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>تنوع در املاک، افزایش قدرت انتخاب مشتریان</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>بانکی جامع از اطلاعات هزاران آگهی به روز</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>دستیابی به نتیجه مطلوب در کمترین زمان ممکن</p>
          </li>
          <li>
            <p><i className="fa fa-check-circle set-purple"/>همکاری با مشاوران متخصص در حوزه املاک</p>
          </li>
        </ul>
      </div>
      <div className="col-md-4 col-8">
        <div className="">
          <img src="/why-khanebedoosh.jpg" width="100%" alt="چرا ما؟"/>
        </div>
      </div>
    </div>
  );
}

function WhyUs() {
  return (
    <div className="container-fluid">
      <WhyUsLogos/>
      <WhyUsText/>
    </div>

  );
}

function Footer() {
  return (
    <footer className="container-fluid footer-mine">
      <div className="row">
        <div className="col-md-9 col-12 footer-right">
          <p>تمام حقوق مادی و معنوی این وب‌سایت متعلق به دانیال و سبحان می‌باشد.</p>
        </div>
        <div className="col-md-3 col-12 footer-left">
          <a href="http://www.instagram.com" className="fa social-logo">
            <img src="/icons/instagram-logo.png" alt="اینستاگرام"/>
          </a>
          <a href="http://www.telegram.com" className="fa social-logo">
            <img src="/icons/telegram-logo.png" alt="تلگرام"/>
          </a>
          <a href="http://www.twitter.com" className="fa social-logo">
            <img src="/icons/twitter-logo.png" alt="توییتر"/>
          </a>
        </div>
      </div>
    </footer>
  );
}

class Landing extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <UpperPart/>
        <WhyUs/>
        <Footer/>
      </div>
    );
  }
}

export {Footer, Landing , Navbar};
