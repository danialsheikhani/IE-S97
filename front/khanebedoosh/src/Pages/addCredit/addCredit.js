import React from 'react';
import './addCredit.css';
import {Area} from '../searchResult/searchResult';
import {Navbar, Footer} from "../landing/landing";

class CreditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      balance: 0,
      credit: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    e.preventDefault();
    let url = 'http://localhost:4000/credit';
    fetch(url, {
      method: 'POST',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': window.sessionStorage.token
      },
      body: JSON.stringify({
        credit: this.state.credit
      })
    })
      .then(res => res.json())
      .then(
        (result) => {location.reload()},
        (error) => {
          console.log(error.toString());
          this.setState({
            error
          });
        }
      );

    // url = 'http://localhost:4000/user';
    // fetch(url, {
    //   headers:{'Authorization': window.sessionStorage.token}
    // })
    //   .then(res => res.json())
    //   .then(
    //     (result) => {
    //       this.setState({
    //         balance: result.balance
    //       });
    //     },
    //     (error) => {
    //       console.log(error.toString());
    //       this.setState({
    //         error
    //       });
    //     }
    //   );
  }
  componentDidMount() {

    const url = 'http://localhost:4000/user';
    fetch(url, {
      headers : {'Authorization': window.sessionStorage.token}
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            balance: result.balance
          });
        },
        (error) => {
          console.log(error.toString());
          this.setState({
            error
          });
        }
      )
  }
  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(
      {[name]: value}
    );
  }
  render() {
    return (
      <div className="text-center" id="credit-form">
        <div className="container-fluid">
          <form>
            <div className="row set-center text-center">
              <div className="col-md-5 col-10">
                <div className="current-credit set-gray">اعتبار کنونی</div>
                <div className="current-credit set-black">{this.state.balance}</div>
                <div className="current-credit set-gray"> تومان</div>
              </div>
              <div className="col-md-5 col-10">
                <div className="row unit-add-credit set-center set-gray">تومان</div>
                <div className="row row-add-credit set-center">
                  <input className="increase-credit-field" type="number" placeholder="مبلغ"
                         name={"credit"} value={this.state.credit}
                         onChange={(event) => this.handleUserInput(event)}/>
                </div>
                <div className="row row-add-credit set-center">
                  <button className="btn-add-credit btn my-button" onClick={this.handleClick}>افزایش اعتبار</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );

  }

}


function AddCredit (){
  return (
    <div>
      <Navbar/>
      <Area text={"افزایش موجودی"}/>
      <br/>
      <br/>
      <br/>
      <CreditForm/>
      <Footer/>
    </div>
  );
}


export {AddCredit};
