import React from 'react';
import '../addCredit/addCredit.css';
import './login.css';
import {Navbar, Footer} from "../landing/landing";
import {Area} from "../searchResult/searchResult";

class LoginForm extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(
      {[name]: value}
    );
  }
  handleClick(e){
    e.preventDefault();
    const url = "http://localhost:4000/login";
    fetch(url , {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
      .then(res => res.json())
      .then(
        (result) => {window.sessionStorage.token = result.token},
        (error) => {
          console.log(error.toString());
          this.setState({error});
        }
      )
      .then(() => window.location.replace('/landing'));
  }
  render() {
    return (
      <div className="text-center" id={"login-form"}>
        <div className="container-fluid">
          <form>
            <div className="row set-center text-center">
              <input className="col-md-3 col-6 increase-credit-field" type="text" placeholder="نام کاربری"
                     name={"username"} value={this.state.username}
                     onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="row set-center text-center">
              <input className="col-md-3 col-6 increase-credit-field" type="password" placeholder="گذرواژه"
                     name={"password"} value={this.state.password}
                     onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div>
              <button className="col-md-3 col-6 btn btn-add-credit" onClick={this.handleClick}>ورود</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

function Login() {
  return (
    <div>
      <Navbar/>
      <Area text={"ورود کاربران"}/>
      <LoginForm/>
      <Footer/>
    </div>
  );
}
export {Login};