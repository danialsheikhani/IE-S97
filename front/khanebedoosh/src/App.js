import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import './App.css';
import {Landing} from './Pages/landing/landing';
import {SearchResult} from './Pages/searchResult/searchResult';
import {AddCredit} from './Pages/addCredit/addCredit';
import {HouseDetails} from './Pages/houseDetails/houseDetails';
import {Login} from "./Pages/login/login";

class App extends React.Component {

  render() {

    return (

      <Router>
        <div>
          <Route path="/landing" component={Landing}/>
          <Route path="/searchResult" component={SearchResult}/>
          <Route path="/addCredit" component={AddCredit}/>
          <Route path="/House" component={HouseDetails}/>
          <Route path="/login" component={Login}/>
        </div>
      </Router>
    );
  }
}

export default App;
