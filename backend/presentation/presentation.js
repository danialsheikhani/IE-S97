const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const domain = require('../domain/domain');
const port = process.env.port || 4000;

//Ports are usually set through enviroment variables
//Set to 3000 if env variable not exists
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(['/user', '/credit', '/buy'], domain.verifyToken);

app.post('/signup', async function (req, res){
  const individualData = {
    name: req.body.name,
    username: req.body.username,
    password: req.body.password,
    balance: req.body.balance,
    phone: req.body.phone,
    admin: req.body.admin,
  };
  const result = await domain.signUp(individualData);
  res.json(result);
});
app.post('/login', async function (req, res) {
  const loginData = {
    username: req.body.username,
    password: req.body.password
  };
  const token = await domain.login(loginData);
  res.json({token});
});
app.post('/credit', async function (req, res) {
  const creditData = {
    credit: req.body.credit,
    userId: req.user.id
  };
  await domain.addCredit(creditData);
  res.json({result: 'OK'});
});
app.get('/user', async function (req, res) {
  const result = await domain.getUser(req.user);
  res.json(result);
});
app.get('/house/:houseId', async function(req, res) {
  const {data} = await domain.getHouseDetails(req.params.houseId);
  res.json(data);
});

app.post('/search', async function (req, res) {
  const searchData = {
    leastArea: req.body.leastArea,
    buildingType: req.body.buildingType,
    dealType: req.body.dealType,
    mostPrice: req.body.mostPrice,
  };
  const result = await domain.search(searchData);
  res.json(result);
});
app.get('/buy/:houseId', async function (req, res) {
  const result = await domain.showOrBuyPhoneNumber(req.params.houseId, req.user.id);
  const response = {
    msg: result
  };
  res.json(response);
});

app.listen(port);
console.log('Listening on port: ' + port);
