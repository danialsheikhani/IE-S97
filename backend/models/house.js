'use strict';
module.exports = (sequelize, DataTypes) => {
  const House = sequelize.define('House', {
    uuid: DataTypes.STRING,
    area: DataTypes.INTEGER,
    address: DataTypes.STRING,
    imageUrl: DataTypes.STRING,
    basePrice: DataTypes.INTEGER,
    rentPrice: DataTypes.INTEGER,
    sellPrice: DataTypes.INTEGER,
    dealType: DataTypes.INTEGER,
    buildingType: DataTypes.STRING,
    phone: DataTypes.STRING,
    description: DataTypes.STRING,
    expireTime: DataTypes.STRING,
    ownerName: DataTypes.STRING
  }, {});
  House.associate = function(models) {
    // associations can be defined here
  };
  return House;
};