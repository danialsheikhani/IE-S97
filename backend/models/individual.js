'use strict';
const sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Individual = sequelize.define('Individual', {
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    phone: DataTypes.STRING,
    balance: DataTypes.INTEGER,
    admin: DataTypes.INTEGER
  }, {});
  Individual.associate = function(models) {
    // associations can be defined here
  };
  return Individual;
};