'use strict';
module.exports = (sequelize, DataTypes) => {
  const PaymentHistory = sequelize.define('PaymentHistory', {
    userId: DataTypes.INTEGER,
    houseId: DataTypes.INTEGER
  }, {});
  PaymentHistory.associate = function(models) {
    // associations can be defined here
  };
  return PaymentHistory;
};