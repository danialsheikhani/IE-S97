'use strict';
const House = require('../models').House;
const Individual = require('../models').Individual;
const PaymentHistory = require('../models').PaymentHistory;
const RealEstate = require('../models').RealEstate;

const config = require('../config/domainConfig');

let functions = {
  async insertIndividual(individualData) {
    return await Individual.create(individualData);
  },
  async fetchIndividualById(userId) {
    return await Individual.findById(userId);
  },
  async fetchIndividualByUsername(username) {
    return await Individual.findOne({
      where: {
        username
      }
    });
  },
  async updateIndividual(individual) {
    return await individual.save();
  },
  async insertHouse(houseData) {
    return await House.create(houseData);
  },
  async fetchHouseById(houseId) {
    return await House.findById(houseId);
  },
  async fetchHouseByFilter(leastArea, buildingType, dealType, mostPrice) {
    return await House.findAll({
      where : {
        dealType: { $eq: dealType },
        buildingType: { $eq: buildingType},
        area: { $gte : leastArea},
        $or : [
          { sellPrice: { $lte: mostPrice } },
          { basePrice: { $lte: mostPrice } }
        ]
      }
    });
  },
  async fetchPaymentRecord(houseId, userId){
    return await PaymentHistory.findAll({
      where: {
        userId: { $eq: userId },
        houseId: { $eq: houseId }
      }
    });
  },
  async updateBalance(userId, newBalance){
    return await Individual.update({
      balance: newBalance,
    }, {
      where: {
        id: { $eq: userId }
      }
    });
  },
  async insertPaymentRecord(recordData){
    return await PaymentHistory.create(recordData);
  },
  async deleteAllHouses(){
    House.destroy({
      where: {},
    });
  }
};

module.exports = functions;