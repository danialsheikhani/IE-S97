'use strict';

const axios = require('axios');

const config = require('../config/domainConfig');

let functions = {
  async fetchHouseDetailsById(houseId) {
    const response = await axios.get(config.houseDetailsBaseUrl + houseId);
    return response.data;
  },
  async fetchHouses(){
    const response = await axios.get(config.houseDetailsBaseUrl);
    return response.data;
  },
  async fetchHousePhoneNumberById(houseId){
    const response = await axios.get(config.houseDetailsBaseUrl + houseId);
    return response.data.data.phone;
  }


};

module.exports = functions;