'use strict';

const serviceDA = require('../dataaccess/serviceDataaccess');
const dataaccess = require('../dataaccess/dataaccess');
const config = require('../config/domainConfig');
const jwt = require('jsonwebtoken');

let privates = {
  expireTime : 0
};

let functions = {
  verifyToken: function (req, res, next) {
    if (req.headers.authorization){
      const { userId } = jwt.verify(req.headers.authorization, config.jwt.secretKey, config.jwt.signOptions);
      req.user = { id: userId };
    }
    next();
  },
  async signUp(individualData) {
    return dataaccess.insertIndividual(individualData);
  },
  async login(loginData) {
    const user = await dataaccess.fetchIndividualByUsername(loginData.username);
    if (user.password === loginData.password){
      return jwt.sign({userId: user.id}, config.jwt.secretKey, config.jwt.signOptions);
    }
  },
  async getUser(user) {
    return await dataaccess.fetchIndividualById(user.id);
  },
  async addCredit(creditData) {
    let {credit, userId} = creditData;
    if (!credit || credit.isNaN || credit < 0){
      throw new Error('Invalid Credit Input');
    }
    let individual = await dataaccess.fetchIndividualById(userId);
    individual.balance = (+individual.balance) + (+credit);
    dataaccess.updateIndividual(individual);
  },
  async search(searchData) {
    const { leastArea, buildingType, dealType, mostPrice } = searchData;
    if (privates.expireTime < Date.now()){
      await dataaccess.deleteAllHouses();
      const response = await serviceDA.fetchHouses();
      privates.expireTime = response.expireTime;
      for (let i=0; i < response.data.length; i++){
        const newHouse = {
          uuid: response.data[i].id,
          area: response.data[i].area,
          dealType: response.data[i].dealType,
          buildingType: response.data[i].buildingType,
          address: response.data[i].address,
          imageUrl: response.data[i].imageURL,
          sellPrice: response.data[i].price.sellPrice,
          basePrice: response.data[i].price.basePrice,
          rentPrice: response.data[i].price.rentPrice,
        };
        dataaccess.insertHouse(newHouse);
      }
    }
    const result = await dataaccess.fetchHouseByFilter(leastArea, buildingType, dealType, mostPrice);
    return result;
  },
  async getHouseDetails(houseId) {
    const response = await serviceDA.fetchHouseDetailsById(houseId);
    if (response.result === 'OK'){
      delete response.data.phone;
      return response;
    }
  },
  async showOrBuyPhoneNumber(houseId, userId) {
    const phoneNumber = await serviceDA.fetchHousePhoneNumberById(houseId);
    const paymentRecord = await dataaccess.fetchPaymentRecord(houseId, userId);
    if(paymentRecord[0]){
      return phoneNumber;
    }
    else{
      let balance = await dataaccess.fetchIndividualById(userId);
      balance = balance.toJSON().balance;
      if (balance >= config.phoneNumberPrice){
        await dataaccess.updateBalance(userId, balance - config.phoneNumberPrice);
        const recordData = {
          houseId,
          userId,
        };
        dataaccess.insertPaymentRecord(recordData);
        return phoneNumber;
      }
      else{
        return null;
      }
    }
  }
};

functions.private = privates;
module.exports = functions;