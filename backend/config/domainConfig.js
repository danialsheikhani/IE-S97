const domainConfig = {
  phoneNumberPrice: 1000,
  houseDetailsBaseUrl: 'http://139.59.151.5:6664/khaneBeDoosh/v2/house/',
  jwt: {
    'secretKey': 'mozi-amoo',
    'signOptions': {
      'algorithm': 'HS512',
      'audience': 'khanebedoosh:app',
      'subject': 'khanebedoosh:token',
      'issuer': 'khanebedoosh:server'
    }
  }
};
module.exports = domainConfig;