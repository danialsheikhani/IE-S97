import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import ir.khaneBeDoosh.Individual;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationFilter implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
        response.setHeader("Access-Control-Allow-Headers", "Authorization");


        String token = request.getHeader("Authorization");
        if (token != null){
            try{
                Claims claims = Jwts.parser()
                        .setSigningKey("mozi-amoo".getBytes("UTF-8"))
                        .requireIssuer("KhaneBeDoosh")
                        .parseClaimsJws(token).getBody();
                String name = claims.get("name", String.class);
                request.setAttribute("name", name);
            } catch (Exception e){
                response.sendError(403);
                e.printStackTrace();
            } finally {
                chain.doFilter(request, response);
            }
        }
    }

    public void destroy() {
        //close any resources here
    }

}