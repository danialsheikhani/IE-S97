import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;

import org.json.JSONObject;

import java.io.*;

public class GetHouseDetailsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("UTF-8");
        String houseId = request.getParameter("houseId");
        JSONObject result = Server.getHouseDetailsById(houseId);
        result.remove("phone");
        PrintWriter out = response.getWriter();
        out.println(result.toString());
    }
}
