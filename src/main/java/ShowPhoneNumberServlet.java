import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;
import org.json.JSONObject;

import java.io.*;

public class ShowPhoneNumberServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String individualName = (String) request.getAttribute("name");
        String houseId = request.getParameter("houseId");
        String msg = Server.showNumber(individualName, houseId);
        PrintWriter out = response.getWriter();
        out.println(
                new JSONObject()
                .put("msg", msg).toString()
        );

    }
}
