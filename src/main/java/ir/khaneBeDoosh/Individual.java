package ir.khaneBeDoosh;

public class Individual extends User{
    private String phone;
    private String username;
    private String password;
    private int balance;
    private boolean admin;

    public Individual(String p, int b, String u, String pass, String n, boolean a){
        phone = p;
        balance = b;
        username = u;
        password = pass;
        name = n;
        admin = a;
    }

    public boolean isAdmin() {
        return admin;
    }
    public int getBalance() {
        return balance;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public String getUsername() {
        return username;
    }

    public boolean pay(String houseId){
        if(balance > Server.cost){
            balance -= Server.cost;
            this.save();
            DataAccess.insertPayment(name, houseId);
            return true;
        }
        return false;
    }
    public void addCredit(int credit){
        balance += credit;
        this.save();
    }
    private void save(){
        DataAccess.updateIndividual(this);
    }

}
