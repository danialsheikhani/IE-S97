package ir.khaneBeDoosh;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

public class DataAccess {
    private static Connection conn = null;
    private static PreparedStatement preparedStatement = null;
    static{
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.sqlite");
            createTables();

        } catch ( Exception e ) {
            System.err.println("dataaccess::staticInit");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

    private static void createTables() throws SQLException{
        String createHouseTableSQL = "CREATE TABLE IF NOT EXISTS HOUSES " +
                "(ID CHAR(40) PRIMARY KEY NOT NULL," +
                " AREA INT, " +
                " BUILDINGTYPE CHAR(20), " +
                " ADDRESS CHAR(20), " +
                " IMAGEURL CHAR(150)," +
                " DEALTYPE INT," +
                " BASEPRICE INT, " +
                " RENTPRICE INT, " +
                " SELLPRICE INT," +
                " PHONE CHAR(20)," +
                " DESCRIPTION CHAR(200)," +
                " EXPIRETIME CHAR(100)," +
                " OWNERNAME CHAR(100));";
        preparedStatement = conn.prepareStatement(createHouseTableSQL);
        preparedStatement.executeUpdate();

        String createIndividualTableSQL = "CREATE TABLE IF NOT EXISTS Individual" +
                "(name VARCHAR PRIMARY KEY NOT NULL," +
                " phone VARCHAR ," +
                " balance INT," +
                " username VARCHAR ," +
                " admin INT ," +
                " password VARCHAR );";

        preparedStatement = conn.prepareStatement(createIndividualTableSQL);
        preparedStatement.executeUpdate();

        String createRealEstatesTableSQL = "CREATE TABLE IF NOT EXISTS REAL_ESTATES" +
                "(NAME CHAR(100) PRIMARY KEY NOT NULL);";

        preparedStatement = conn.prepareStatement(createRealEstatesTableSQL);
        preparedStatement.executeUpdate();


        String createPaymentHistoryTableSQL = "CREATE TABLE IF NOT EXISTS PaymentHistory" +
                "(name varchar NOT NULL," +
                " houseId CHAR(40) NOT NULL," +
                " PRIMARY KEY(name, houseId)," +
                " FOREIGN KEY(name) REFERENCES Individual," +
                " FOREIGN KEY(houseId) REFERENCES HOUSES);";

        preparedStatement = conn.prepareStatement(createPaymentHistoryTableSQL);
        preparedStatement.executeUpdate();

    }
    public static Individual getIndividualByName(String name){
        try {
            String selectString = "SELECT * FROM Individual WHERE name = ?";
            preparedStatement = conn.prepareStatement(selectString);
            preparedStatement.setString(1, name);

            ResultSet rs = preparedStatement.executeQuery();
            Individual individual = new Individual(rs.getString("phone"),
                    rs.getInt("balance"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("name"),
                    rs.getBoolean("admin"));
            return individual;
        } catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return null;
    }
    public static boolean isPaid(String name, String houseId){
        try {
            String sql = "SELECT (COUNT(*) > 0) FROM PaymentHistory WHERE name = ? AND houseId = ? ";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, houseId);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                boolean found = rs.getBoolean(1); // "found" column
                if (found) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        return false;
    }
    public static void updateIndividual(Individual individual){
        try{
            String sql = "UPDATE Individual SET phone = ?, balance = ?, username = ?, admin = ?, password = ? WHERE name = ?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, individual.getPhone());
            preparedStatement.setInt(2, individual.getBalance());
            preparedStatement.setString(3, individual.getUsername());
            preparedStatement.setInt(4, individual.isAdmin()? 1 : 0);
            preparedStatement.setString(5, individual.getPassword());
            preparedStatement.setString(6, individual.getName());
            preparedStatement.executeUpdate();
        } catch (Exception e){
            System.err.println("dataaccess::updateIndividual");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    public static void insertPayment(String name, String houseId){
        try {
            String sql = "INSERT INTO PaymentHistory (name, houseId)" +
                         "VALUES (?,?);";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, houseId);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            System.err.println("dataaccess::insertPayment");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    public static House getHouseById(String houseId){
        try {
            String sql = "SELECT * FROM HOUSES WHERE ID = ? ";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, houseId);
            ResultSet rs = preparedStatement.executeQuery();
            House house = new House(rs.getString("ID"),
                    rs.getInt("AREA"),
                    rs.getString("BUILDINGTYPE"),
                    rs.getString("ADDRESS"),
                    rs.getString("IMAGEURL"),
                    rs.getInt("DEALTYPE"),
                    rs.getInt("BASEPRICE"),
                    rs.getInt("RENTPRICE"),
                    rs.getInt("SELLPRICE"),
                    rs.getString("PHONE"),
                    rs.getString("DESCRIPTION"),
                    rs.getString("EXPIRETIME"),
                    rs.getString("OWNERNAME"));
            return house;
        } catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return null;
    }
    public static void addHouse(House house){
        try {
            String SQL_INSERT = "INSERT INTO HOUSES (ID, AREA, BUILDINGTYPE, ADDRESS, IMAGEURL, DEALTYPE, BASEPRICE, RENTPRICE, SELLPRICE, PHONE, DESCRIPTION, EXPIRETIME, OWNERNAME)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            preparedStatement = conn.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, house.getId());
            preparedStatement.setInt(2, house.getArea());
            preparedStatement.setString(3,house.getBuildingType());
            preparedStatement.setString(4, house.getAddress());
            preparedStatement.setString(5, house.getImageURL());
            preparedStatement.setInt(6, house.getDealType());
            preparedStatement.setInt(7, house.getBasePrice());
            preparedStatement.setInt(8, house.getRentPrice());
            preparedStatement.setInt(9, house.getSellPrice());
            preparedStatement.setString(10 , house.getPhone());
            preparedStatement.setString(11, house.getDescription());
            preparedStatement.setString(12, house.getExpireTime());
            preparedStatement.setString(13, house.getOwnerName());

            preparedStatement.addBatch();
        } catch(Exception e){
            System.err.println("dataaccess::addHouse");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    public static void addHouses(List<House> houses) throws SQLException {
        String SQL_INSERT = "INSERT INTO HOUSES (ID, AREA, BUILDINGTYPE, ADDRESS, IMAGEURL, DEALTYPE, BASEPRICE, RENTPRICE, SELLPRICE, PHONE, DESCRIPTION, EXPIRETIME, OWNERNAME)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        conn.setAutoCommit(false);
        preparedStatement = conn.prepareStatement(SQL_INSERT);

        for(int i=0; i < houses.size(); i++){
            House house = houses.get(i);
            try{
                preparedStatement.setString(1, house.getId());
                preparedStatement.setInt(2, house.getArea());
                preparedStatement.setString(3,house.getBuildingType());
                preparedStatement.setString(4, house.getAddress());
                preparedStatement.setString(5, house.getImageURL());
                preparedStatement.setInt(6, house.getDealType());
                preparedStatement.setInt(7, house.getBasePrice());
                preparedStatement.setInt(8, house.getRentPrice());
                preparedStatement.setInt(9, house.getSellPrice());
                preparedStatement.setString(10 , house.getPhone());
                preparedStatement.setString(11, house.getDescription());
                preparedStatement.setString(12, house.getExpireTime());
                preparedStatement.setString(13, house.getOwnerName());

                preparedStatement.addBatch();
            } catch (SQLException e) {
                System.err.println("dataacess:addHouses");
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            }
            System.out.println(i + " : " + System.currentTimeMillis());
        }
        preparedStatement.executeBatch();
        System.out.println("before commit : " + System.currentTimeMillis());
        conn.commit();
        System.out.println("after commit : " + System.currentTimeMillis());
        conn.setAutoCommit(true);
    }
    public static List<House> searchHouse(int leastArea, int mostPrice, int dealType, String buildingType){
        List<House> result = new ArrayList<>();
        try {
            String sql = "";
            if ( dealType == 0 )
                sql = "SELECT * FROM HOUSES WHERE AREA > ? AND SELLPRICE < ? AND DEALTYPE = ? AND BUILDINGTYPE = ?";
            if ( dealType == 1 )
                sql = "SELECT * FROM HOUSES WHERE AREA > ? AND BASEPRICE < ? AND DEALTYPE = ? AND BUILDINGTYPE = ?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, leastArea);
            preparedStatement.setInt(2, mostPrice);
            preparedStatement.setInt(3, dealType);
            preparedStatement.setString(4, buildingType);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()){
                House temp = new House(rs.getString("ID"),
                        rs.getInt("AREA"),
                        rs.getString("BUILDINGTYPE"),
                        rs.getString("ADDRESS"),
                        rs.getString("IMAGEURL"),
                        rs.getInt("DEALTYPE"),
                        rs.getInt("BASEPRICE"),
                        rs.getInt("RENTPRICE"),
                        rs.getInt("SELLPRICE"),
                        rs.getString("PHONE"),
                        rs.getString("DESCRIPTION"),
                        rs.getString("EXPIRETIME"),
                        rs.getString("OWNERNAME"));
                result.add(temp);
            }
        } catch(Exception e){
            System.err.println("dataaccess::searchHouse");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return result;
    }
    public static void deleteHouses(){
        try {
            String sql = "DELETE FROM HOUSES;";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch(Exception e){
            System.err.println("dataaccess::deleteHouses");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

    }
    public static void main( String args[] ) {
        try{
            String sql = "INSERT OR REPLACE INTO Individual (name, phone, balance, username, password, admin)" +
                    "VALUES ('danial', '989196833347', 2000, 'danial sheikhani', '12345678', '0');";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Table created successfully");

    }
    public static List<String> fetchUserPaidHouses (String individualName){
        List<String> result = new ArrayList<>();
        try {
            String sql = "SELECT houseId FROM PaymentHistory Where name = ?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, individualName);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                result.add(rs.getString("houseId"));
            }
        } catch (Exception e){
            System.err.println("dataaccess::fetchUserPaidHouses");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return result;
    }
    public static List<String> fetchIndividualsNames(){
        List<String> result = new ArrayList<>();
        String sql = "SELECT name FROM Individual";
        try {
            preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                 result.add(rs.getString("name"));
            }
        } catch (Exception e){
            System.err.println("dataaccess::fetchIndividuals");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return result;
    }


}
