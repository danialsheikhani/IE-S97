package ir.khaneBeDoosh;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

public class Server {

    public static final int cost = 1000;
    private static List<Individual> individuals;
    private static List<House> searchResult;
    private static int h_count;
    static {
        individuals = new ArrayList<>();
        h_count = 0;
        individuals.add(new Individual("+989196833347", 2500, "sobhan96", "1234", "sobhan", false));
        individuals.add(new Individual("+989191234567", 1000, "ehsan88", "bulldozer", "ehsan", false));
    }
    public static JSONObject getHouses(String individualName){
        Individual individual = DataAccess.getIndividualByName(individualName);
        JSONObject result = new JSONObject();
        if (individual.isAdmin()){
            for (String iName : DataAccess.fetchIndividualsNames()){
                JSONArray ja = new JSONArray();
                List<String> paidHousesIds = DataAccess.fetchUserPaidHouses(iName);
                for (String id : paidHousesIds){
                    ja.put(getHouseDetailsById(id));
                }
                result.put(iName, ja);
            }
        } else {
            JSONArray ja = new JSONArray();
            List<String> paidHousesIds = DataAccess.fetchUserPaidHouses(individualName);
            for (String id : paidHousesIds){
                ja.put(getHouseDetailsById(id));
            }
            result.put("data", ja);
        }
        return result;
    }
    private static String sendGet(String address) throws Exception {
        URL url = new URL(address);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
    private static String sendPost(String address, String payload) throws Exception {
        URL url = new URL(address);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true); // triggers it to be POST
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("apiKey", "30ba29e0-0fee-11e8-87b4-496f79ef1988");

        try (OutputStream os = connection.getOutputStream()) {
            os.write(payload.getBytes());
        }
        System.out.println(connection.getResponseCode());
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
    public static void addHouse(String buildingType, int area, int dealType, int price, String address, String phone, String description) {

        int rentPrice = 0, sellPrice = 0, basePrice = 0;
        if (dealType == 0) { // sell
            rentPrice = 0;
            sellPrice = price;
            basePrice = 0;
        } else if (dealType == 1) { // rent
            rentPrice = price;
            sellPrice = 0;
            basePrice = 0;
        }
        House temp = new House(Integer.toString(h_count), area, buildingType, address, "", dealType, basePrice, rentPrice, sellPrice, phone, description, null, null);
        h_count++;
        DataAccess.addHouse(temp);
    }
    public static List<House> getHousesResult() {
        return searchResult;
    }
    public static void searchHouse(int leastArea, int mostPrice, int dealType, String buildingType) {
        searchResult = new ArrayList<>();
        try {
            getHousesFromRealEstates(sendGet("http://139.59.151.5:6664/khaneBeDoosh/v2/house"));
        } catch (java.lang.Exception e) {
            System.out.println("Couldn't reach BONGAH!");
        }
        searchResult = DataAccess.searchHouse(leastArea, mostPrice, dealType, buildingType);
    }
    public static JSONObject getProfileData(String name) {
        Individual individual = DataAccess.getIndividualByName(name);
        return new JSONObject()
                .put("username",individual.getUsername())
                .put("balance", individual.getBalance());
    }
    private static void getHousesFromRealEstates(String response) throws JSONException, SQLException {

        List<House> tempHouses = new ArrayList<>();
        final JSONObject obj = new JSONObject(response);
        Timestamp expireTime = new Timestamp(obj.getLong("expireTime"));
        final JSONArray data = obj.getJSONArray("data");
        final int dataLength = data.length();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        System.out.println("expireTime : " + expireTime.getTime());
        System.out.println("now : " + now.getTime());
        if ( now.after(expireTime)) {
            System.out.println("Using old data");
            return;
        }
        System.out.println("Getting new data");
        DataAccess.deleteHouses();
        for (int i = 0; i < dataLength; i++) {
            final JSONObject jsonObject = data.getJSONObject(i);
            if (jsonObject.getInt("dealType") == 0) {
                House temp = new House(jsonObject.getString("id"),
                        jsonObject.getInt("area"),
                        jsonObject.getString("buildingType"),
                        jsonObject.getString("address"),
                        jsonObject.getString("imageURL"),
                        jsonObject.getInt("dealType"),
                        0,
                        0,
                        jsonObject.getJSONObject("price").getInt("sellPrice"),
                        "",
                        "",
                        "",
                        "");
                tempHouses.add(temp);
            }
            if (jsonObject.getInt("dealType") == 1) {
                House temp = new House(jsonObject.getString("id"),
                        jsonObject.getInt("area"),
                        jsonObject.getString("buildingType"),
                        jsonObject.getString("address"),
                        jsonObject.getString("imageURL"),
                        jsonObject.getInt("dealType"),
                        jsonObject.getJSONObject("price").getInt("basePrice"),
                        jsonObject.getJSONObject("price").getInt("rentPrice"),
                        0,
                        "",
                        "",
                        "",
                        "");
                tempHouses.add(temp);
            }
        }
        DataAccess.addHouses(tempHouses);
    }
    public static String getUTF8Charset(String p) {
        byte[] bytes = p.getBytes(StandardCharsets.ISO_8859_1);
        return new String(bytes, StandardCharsets.UTF_8);
    }
    public static JSONObject getHouseDetailsById(String id) throws JSONException {
            if (id.length() > 3) {
                String response = "";
                try {
                    response = sendGet("http://139.59.151.5:6664/khaneBeDoosh/v2/house/" + id);
                } catch (Exception e) {
                    System.out.println("Couldn't reach House!");
                }
                return new JSONObject(response).getJSONObject("data");
            }
            else{
                return new JSONObject(DataAccess.getHouseById(id));
            }
    }
    public static void addCredit(int credit, String username) throws Exception {
        String payload = new JSONStringer()
                .object()
                .key("userId")
                .value(username)
                .key("value")
                .value(credit)
                .endObject()
                .toString();
        String response = sendPost("http://139.59.151.5:6664/bank/pay", payload);
        if (new JSONObject(response).get("result").equals("OK")) {
            Individual user = DataAccess.getIndividualByName(username);
            user.addCredit(credit);

        }
    }
    public static String showNumber(String iName, String h) {
        if (DataAccess.isPaid(iName, h)){
            return getHouseDetailsById(h).getString("phone");
        }
        Individual i = DataAccess.getIndividualByName(iName);
        if (i.pay(h)){
            return getHouseDetailsById(h).getString("phone");
        }
        return "اعتبار شما کافی نیست!";
    }
    public static JSONObject login (String name, String password) throws Exception {
        Individual individual = DataAccess.getIndividualByName(name);
        JSONObject result = new JSONObject();
        if ( password.equals(individual.getPassword())){
            try {
                String jwt = Jwts.builder()
                        .setIssuedAt(new Date())
                        .setIssuer("KhaneBeDoosh")
                        .claim("name", individual.getName())
                        .signWith(
                                SignatureAlgorithm.HS256,
                                "mozi-amoo".getBytes("UTF-8")
                        )
                        .compact();
                result.put("token", jwt);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        else {
            throw new Exception("server::login::WRONG_PASSWORD");
        }
        return result;
    }
}

