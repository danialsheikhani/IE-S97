package ir.khaneBeDoosh;

public class House {
    private String id;
    private int area;
    private String buildingType;
    private String address;
    private String imageURL;
    private int dealType;
    private int basePrice;
    private int rentPrice;
    private int sellPrice;
    private String phone;
    private String description;
    private String expireTime;
    private String ownerName;

    public House(String id, int area, String buildingType, String address, String imageURL, int dealType, int basePrice, int rentPrice, int sellPrice, String phone, String description, String expireTime, String ownerName) {
        this.id = id;
        this.area = area;
        this.buildingType = buildingType;
        this.address = address;
        this.imageURL = imageURL;
        this.dealType = dealType;
        this.basePrice = basePrice;
        this.rentPrice = rentPrice;
        this.sellPrice = sellPrice;
        this.phone = phone;
        this.description = description;
        this.expireTime = expireTime;
        this.ownerName = ownerName;
    }
    public House(){

    }

    public String getId() {
        return id;
    }

    public int getArea() {
        return area;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public String getAddress() {
        return address;
    }

    public String getImageURL() {
        return imageURL;
    }

    public int getDealType() {
        return dealType;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public int getRentPrice() {
        return rentPrice;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public String getOwnerName(){
        return ownerName;
    }







    public void setId(String id) {
        this.id = id;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setDealType(int dealType) {
        this.dealType = dealType;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public void setRentPrice(int rentPrice) {
        this.rentPrice = rentPrice;
    }

    public void setSellPrice(int sellPrice) {
        this.sellPrice = sellPrice;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}
