import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;
import org.json.JSONObject;

import java.io.*;

public class GetHousesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String individualName = (String) request.getAttribute("name");
        JSONObject result = Server.getHouses(individualName);
        PrintWriter out = response.getWriter();
        out.println(result.toString());

    }
}
