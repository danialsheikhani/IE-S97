import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;

import java.io.*;

public class AddCreditServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        int credit = Integer.parseInt(request.getParameter("credit"));
        String name = (String) request.getAttribute("name");
        try {
            Server.addCredit(credit, name);
        } catch (Exception e){
            System.err.println("AddCreditServlet");
        }
        PrintWriter out = response.getWriter();
        out.println("{}");
    }
}
