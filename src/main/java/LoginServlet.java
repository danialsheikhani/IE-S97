import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;
import org.json.JSONObject;

import java.io.*;

public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("UTF-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        try {
            JSONObject result = Server.login(username, password);
            PrintWriter out = response.getWriter();
            out.println(result.toString());
        } catch (Exception e){
            response.sendError(403);
            e.printStackTrace();
            System.err.println("login failed");
        }

    }
}
