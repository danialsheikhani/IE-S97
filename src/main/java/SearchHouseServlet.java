import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ir.khaneBeDoosh.*;

import org.codehaus.jackson.map.ObjectMapper;
import java.io.*;

public class SearchHouseServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("UTF-8");
        int leastArea = Integer.parseInt(request.getParameter("leastArea"));
        int mostPrice = Integer.parseInt(request.getParameter("mostPrice"));
        int dealType = Integer.parseInt(request.getParameter("dealType"));
        String buildingType = Server.getUTF8Charset(request.getParameter("buildingType"));
        Server.searchHouse(leastArea, mostPrice, dealType, buildingType);
        PrintWriter out = response.getWriter();
        out.println("[");

        int arr_size = Server.getHousesResult().size();
        for(int i=0; i < arr_size; i++){
            ObjectMapper mapper = new ObjectMapper();
            House obj = Server.getHousesResult().get(i);
            try {
                String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
                out.println(jsonInString);
                if ( i < arr_size - 1)
                    out.println(",");

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        out.println("]");


    }
}
